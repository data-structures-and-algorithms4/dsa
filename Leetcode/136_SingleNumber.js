// Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
// You must implement a solution with a linear runtime complexity and use only constant extra space.
//Solution
var singleNumber = function (nums) {
    let results = {};

    for (const num of nums) {
        if (!(num in results)) results[num] = 0;
        results[num] += 1;
    }

    for (const key in results) {
        if (results[key] === 1) return key;
    }
    return null;
};
