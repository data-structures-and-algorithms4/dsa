function missingNumber(nums) {
    let counter = 0;
    for (let i = 0; i < nums.length; i++) {
        counter++;
        if (!nums.includes(i)) {
            return i;
        }
    }
    if (counter == nums.length) {
        return counter;
    }
}
console.log(missingNumber([9, 2, 4, 3, 8, 5, 6, 0, 7, 1]));
