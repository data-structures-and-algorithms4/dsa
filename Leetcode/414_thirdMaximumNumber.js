// Given an integer array nums, return the third distinct maximum number in this array.
//  If the third maximum does not exist, return the maximum number.
// Example - Input: nums = [3,2,1]
// Output: 1
// Explanation:
// The first distinct maximum is 3.
// The second distinct maximum is 2.
// The third distinct maximum is 1.
//
// Example 2:
// Input: nums = [1,2]
// Output: 2
// Explanation:
// The first distinct maximum is 2.
// The second distinct maximum is 1.
// The third distinct maximum does not exist, so the maximum (2) is returned instead.
//
//SOLUTION 1
var thirdMax = function (nums) {
    let mapNums = [];
    let counter = 0;
    let uniqueNums = new Set(nums);
    const size = uniqueNums.size;

    while (counter < size) {
        const maxNum = Math.max(...uniqueNums);
        mapNums.push(maxNum);
        uniqueNums.delete(maxNum);
        counter++;
    }
    return mapNums.length > 3 ? mapNums[2] : mapNums[mapNums.length - 1];
};
//
// SOLUTION 2
var thirdMax = function (nums) {
    const newSet = new Set(nums);
    const sortedNums = [...newSet].sort((a, b) => b - a);
    return sortedNums.length >= 3 ? sortedNums[2] : sortedNums[0];
};

// console.log(thirdMax([3, 2, 4, 7]));
