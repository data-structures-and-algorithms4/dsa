// Given an array of characters chars, compress it using the following algorithm:

// Begin with an empty string s. For each group of consecutive repeating characters in chars:

// If the group's length is 1, append the character to s.
// Otherwise, append the character followed by the group's length.
// The compressed string s should not be returned separately, but instead, be stored in the input character array chars. Note that group lengths that are 10 or longer will be split into multiple characters in chars.

// After you are done modifying the input array, return the new length of the array.

// You must write an algorithm that uses only constant extra space.
//
//***********************************************************/
//Solution
/**
 * @param {character[]} chars
 * @return {number}
 */
var compress = function (chars) {
    //Initialize and end window, and a counter to store the number of times a char appears
    let windowEnd = 1,
        counter = 0;
    chars.push("end");

    while (chars[windowEnd - 1] !== "end") {
        if (chars[windowEnd] === chars[windowEnd - 1]) {
            counter++;
            chars.shift();
        } else {
            counter++;
            chars.push(chars[0]);
            chars.shift();
            if (counter > 1) chars.push(...(counter + ""));
            counter = 0;
        }
    }
    chars.shift();
    return chars.length;
};
