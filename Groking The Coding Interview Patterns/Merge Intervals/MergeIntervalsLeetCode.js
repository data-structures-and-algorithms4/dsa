/**
 * @param {number[][]} intervals
 * @return {number[][]}
 */
var merge = function (intervals) {
    intervals.sort((a, b) => {
        a[0] - b[0];
    });
    let results = [];
    let curStart, curEnd, prevEnd;

    results.push(intervals[0]);
    for (let interval of intervals) {
        curStart = interval[0];
        curEnd = interval[1];
        prevEnd = results[results.length - 1][1];
        prevStart = results[results.length - 1][0];

        if (curStart <= prevStart)
            results[results.length - 1][0] = Math.min(prevStart, curStart);

        if (prevEnd >= curStart && curStart >= prevStart) {
            results[results.length - 1][1] = Math.max(curEnd, prevEnd);
        } else {
            results.push(interval);
        }
    }
    return results;
};

//
//
