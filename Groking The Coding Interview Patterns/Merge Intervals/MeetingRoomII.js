import { Interval } from "/opt/node/lib/lintcode/index.js";

/**
 * Definition of Interval:
 * class Interval {
 *   constructor(start, end) {
 *     this.start = start;
 *     this.end = end;
 *   }
 * }
 */

export class Solution {
    /**
     * @param intervals: an array of meeting time intervals
     * @return: the minimum number of conference rooms required
     */
    minMeetingRooms(intervals) {
        if (!intervals || intervals.length < 1) return 0;

        const startTime = intervals
            .map((interval) => interval[0])
            .sort((a, b) => {
                a - b;
            });
        const endTime = intervals
            .map((interval) => interval[1])
            .sort((a, b) => {
                a - b;
            });

        let rooms = 0;
        let endIdx = 0;

        for (let i = 0; i < intervals.length; i++) {
            if (startTime[i] < endTime[endIdx]) {
                rooms++;
            } else {
                endIdx++;
            }
        }
        return rooms;
    }
}
