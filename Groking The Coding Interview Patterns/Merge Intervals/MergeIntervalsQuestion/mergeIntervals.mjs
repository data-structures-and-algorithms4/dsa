import Interval from "./interval.mjs";

function mergeIntervals(v) {
    // Your code will replace the placeholder return statement.
    let result = [];
    let curStart, curEnd, prevEnd, lastAddedInterval;
    //Push first interval into results and check against current interval
    result.push(new Interval(v[0].start, v[0].end));
    //Loop through intervals and check
    for (let i = 1; i < v.length; i++) {
        lastAddedInterval = result[result.length - 1];
        curStart = v[i].start;
        curEnd = v[i].end;
        prevEnd = lastAddedInterval.end;

        //Checking overlapping conditions
        if (prevEnd >= curStart) {
            lastAddedInterval.end = Math.max(prevEnd, curEnd);
        } else {
            //Not overlapping
            result.push(new Interval(curStart, curEnd));
        }
    }
    return result;
}

console.log(
    mergeIntervals([
        [3, 7],
        [6, 8],
        [10, 12],
        [11, 15],
    ])
);

// Run with node --experimental-modules mergeIntervals.mjs
