The subsets pattern is useful in finding the permutations and combinations of elements in a data structure.

The idea is to consider the data structure as a set and make a series of subsets from this set.
The subsets generated are based on certain conditions that the problem provides us.

This approach makes all the subsets of a given set in O(n * 2^n) time, where n
is the number of elements in the data structure from which we have to generate subsets.