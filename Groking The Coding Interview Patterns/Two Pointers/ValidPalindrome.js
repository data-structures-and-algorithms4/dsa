// Write a function that takes a string s as input and checks whether it’s a palindrome or not.
//
// The time complexity is O(n) where n is the number of characters present in the string.
// The space complexity is O(1) because we use constant space to store two indices.

const validPalindrome = (s) => {
    let left = 0,
        right = s.length - 1;

    while (left <= right) {
        if (s[left] !== s[right]) {
            return false;
        }
        left++;
        right--;
    }
    return true;
};

console.log(validPalindrome("hello"));
