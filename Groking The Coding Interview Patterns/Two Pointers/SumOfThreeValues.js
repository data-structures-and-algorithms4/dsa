// Given an array of integers, nums, and an integer value, target, determine if there are any three integers in nums whose sum equals the target.
// Return TRUE if three such integers are found in the array. Otherwise, return FALSE.
//
//Solution

const sumOfThree = (nums, target) => {
    //Sort the nums
    nums = nums.sort((a, b) => {
        return a - b;
    });

    //loop through the array to the last but two elements since the last elements will be occupied automatically by the last two elements;
    for (let i = 0; i < nums.length - 2; i++) {
        //Define low as i + 1 and high as the last element in the array
        let low = i + 1,
            high = nums.length - 1;

        while (low < high) {
            let sum = nums[i] + nums[low] + nums[high];

            if (sum === target) {
                return true;
            } else if (sum < target) low++;
            else high--;
        }
    }
};

console.log(sumOfThree([3, 7, 1, 2, 8, 4, 5], 11));
