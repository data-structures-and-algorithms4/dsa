// A valid BST is defined as follows:

// The left subtree of a node contains only nodes with keys less than the node's key.
// The right subtree of a node contains only nodes with keys greater than the node's key.
// Both the left and right subtrees must also be binary search trees.
var isValidBST = function (root) {
    return dfs(root, -Infinity, Infinity);
};

function dfs(node, min, max) {
    if (!node) return true;

    if (node.val <= min || node.val >= max) return false;

    return dfs(node.left, min, node.val) && dfs(node.right, node.val, max);
}
