// You're given an integer total and a list of integers called coins. The variable coins hold a list of coin denominations, and total is the total amount of money.
// You have to find the minimum number of coins that can make up the total amount by using any combination of the coins.
// If the amount can't be made up, return -1. If the total amount is 0, return 0.
//Note: You may assume that we have an infinite number of each kind of coin.
//
//
//Solution
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function (coins, amount) {
    let dp = Array(amount + 1).fill(Infinity);

    //Base case for zero coin
    dp[0] = 0;

    //Iterate through the dp array and coins to get the number of coins for each amount
    for (let i = 1; i <= amount; i++) {
        for (let coin of coins) {
            //Check if there's a coin to generate the current amount
            if (i - coin >= 0) {
                dp[i] = Math.min(dp[i], 1 + dp[i - coin]);
            }
        }
    }
    return dp[amount] > amount ? -1 : dp[amount];
};
