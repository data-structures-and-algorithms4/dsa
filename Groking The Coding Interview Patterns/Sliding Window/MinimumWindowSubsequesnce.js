// Given strings str1 and str2, find the minimum (contiguous) substring subStr of str1,
//  such that every character of str2 appears in subStr in the same order as it is present in str2.

//Time Complexity
// Since we are using two pointers that traverse the entire string only once, the time complexity is O(n)
//  where n is the number of characters in str1.
//
//Space Complexity
// Since we are not using any extra space apart from a few variables, the space complexity is O(1)

// Solution
const minWindowSubsequence = (str1, str2) => {
    let sizeStr1 = str1.length,
        sizeStr2 = str2.length;

    //Initialize length to a high number
    let length = Infinity;

    //Initialize pointers to zero and the minSubSequence to an empty string
    let indexStr1 = 0,
        indexStr2 = 0,
        minSubSequence = "";

    //Processing every character in str1
    while (indexStr1 < sizeStr1) {
        //Check if index character in str1 is equal to the character in str2
        if (str1[indexStr1] == str2[indexStr2]) {
            indexStr2++;

            //Check if indexStr2 has reached the end of str2
            if (indexStr2 == sizeStr2) {
                let start = indexStr1,
                    end = indexStr1 + 1;

                // Initialize start to the index where all characters of str2 were present in str1
                indexStr2--;
                //Decrement pointer indexStr2 and start a reverse loop
                while (indexStr2 >= 0) {
                    //Decrement pointer of indexStr2 until all characters of str2 are found in str1
                    if (str1[start] == str2[indexStr2]) indexStr2 -= 1;
                    //Decrement start pointer every time to find the starting point of the required subsequence
                    start -= 1;
                }
                start++;

                // Check if length of sub sequence pointed by start and end pointers is less than current min length
                if (end - start < length) {
                    // Update length if current sub sequence is shorter
                    length = end - start;
                    // Update minimum subsequence string to this new shorter string
                    minSubSequence = str1.substring(start, end);
                    console.log(minSubSequence);
                }
                // Set indexS1 to start to continue checking in str1 after this discovered subsequence
                indexStr1 = start;
                indexStr2 = 0;
            }
        }
        // Increment the str1 pointer to check next character
        indexStr1++;
    }

    return minSubSequence;
};

console.log(minWindowSubsequence("abcdaebdbbde", "bde"));
