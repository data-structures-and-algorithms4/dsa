//Given a string, find the length of the longest substring in it with no more than K distinct characters.
//Time Complexity O(N) The outer for loop runs for all characters, and the inner while loop processes each
//  character only once; therefore, the time complexity of the algorithm will be O(N+N),
// which is asymptotically equivalent to O(N) O(N).
// | Space O(K) where k is the number of characters in the hashmap
// function longest_substring_with_k_distinct(str, k) {
//     let maxLength = 0,
//         windowStart = 0,
//         charFrequency = {};

//     // Loop through the string to open or close the window
//     for (let windowEnd = 0; windowEnd < str.length; windowEnd++) {
//         const rightChar = str[windowEnd];
//         if (!(rightChar in charFrequency)) {
//             charFrequency[rightChar] = 0;
//         }
//         charFrequency[rightChar] += 1;

//         // Shrink the window till we are left with k distinct chars
//         while (Object.keys(charFrequency).length > k) {
//             const leftChar = str[windowStart];
//             charFrequency[leftChar] -= 1;
//             if (charFrequency[leftChar] === 0) {
//                 delete charFrequency[leftChar];
//             }
//             windowStart += 1;
//         }
//         maxLength = Math.max(maxLength, windowEnd - windowStart + 1);
//     }
//     return maxLength;
// }

console.log(
    `Length of the longest substring: ${longest_substring_with_k_distinct(
        "cbbebi",
        2
    )}`
);

function longest_substring_with_k_distinct(str, k) {
    let windowStrStart = 0,
        maxLength = 0,
        charFreq = {};

    // Loop through the strings to check the longest substring with k distinct chars
    for (let windowStrEnd = 0; windowStrEnd < str.length; windowStrEnd++) {
        const rightChar = str[windowStrEnd];
        if (!(rightChar in charFreq)) {
            charFreq[rightChar] = 0;
        }
        charFreq[rightChar] += 1;

        // check to add and remove chars whiles traversing the string
        while (Object.keys(charFreq).length > k) {
            let leftChar = str[windowStrStart];
            charFreq[leftChar] -= 1;
            if (charFreq[leftChar] === 0) {
                delete charFreq[leftChar];
            }
            windowStrStart += 1;
        }
        maxLength = Math.max(maxLength, windowStrEnd - windowStrStart + 1);
    }
    return maxLength;
}
