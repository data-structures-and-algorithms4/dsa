//Given an array of positive integers and a number ‘S,’ find the length of the smallest contiguous subarray whose sum is greater than or equal to ‘S’.
// Return 0 if no such subarray exists.
//
//Sliding window approach
function smallSubArrayWithGreaterSum(arr, k) {
    let minLength = Infinity;
    let currentSum = 0;
    let windowStart = 0;

    for (let windowEnd = 0; windowEnd < arr.length; windowEnd++) {
        currentSum += arr[windowEnd];
        while (currentSum >= k) {
            minLength = Math.min(minLength, windowEnd - windowStart + 1);
            currentSum -= arr[windowStart];
            windowStart++;
        }
    }
    if (minLength == Infinity) {
        return 0;
    }
    return minLength;
}

console.log(smallSubArrayWithGreaterSum([2, 1, 5, 2, 3, 2], 7));
