function maximumNumber(nums, windowSize) {
    let result = [];
    let window = [];

    if (windowSize > nums.length) return nums.length;

    for (let i = 0; i < windowSize; i++) {
        while (window.length && nums[i] > nums[window[window.length - 1]]) {
            window.pop();
        }
        window.push(i);
    }
    result.push(nums[window[0]]);

    for (let p = windowSize; p < nums.length; p++) {
        //Remove the first element if it's out of the window
        if (window.length && window[0] <= p - windowSize) window.shift();

        while (window.length && nums[p] >= nums[window[window.length - 1]]) {
            window.pop();
        }
        window.push(p);
        result.push(nums[window[0]]);
    }
    return result;
}

console.log(
    maximumNumber(
        [1, 2, 9, 23, -1, 34, 56, 67, -1, -4, -8, -2, 9, 10, 34, 67],
        4
    )
);

// Hence, the overall time complexity of the solution, irrespective of the input, is O(w+n)
// The space complexity of this solution is O(w), where w is the size of the input array.
