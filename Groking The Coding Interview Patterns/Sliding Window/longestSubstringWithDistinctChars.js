// Given a string, find the chars of the longest substring, which has all distinct characters.
var longestPalindrome = function (s) {
    let windowStart = 0;
    let charSet = new Set();
    let maxLength = 0;

    for (let windowEnd = 0; windowEnd < s.length; windowEnd++) {
        if (charSet.has(s[windowEnd])) {
            delete charSet[s[windowStart]];
            windowStart += 1;
        }
        charSet.add(s[windowEnd]);
        maxLength = Math.max(maxLength, windowEnd - windowStart + 1);
    }
    return maxLength;
};

console.log(longestPalindrome("abbbbbb"));
