// Given an array where the element at the index i represents the price of a stock on day i,
// find the maximum profit that you can gain by buying the stock once and then selling it.

const maxProfit = (array) => {
    let maxProfit = 0,
        left = 0,
        right = 1;
    while (right < array.length) {
        if (array[left] < array[right]) {
            maxProfit = Math.max(maxProfit, array[right] - array[left]);
        } else {
            left = right;
        }
        right++;
    }
    return maxProfit;
};
