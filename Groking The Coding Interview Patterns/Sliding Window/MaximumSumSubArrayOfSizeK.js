//Given an array, find the average of all subArrays of ‘K’ contiguous elements in it.
//Brute-Force Approach
// function subArrayAverage(k, arr) {
//     const result = [];
//     for (let idx = 0; idx < arr.length - k + 1; idx++) {
//         let sum = 0.0;
//         for (let j = idx; j < idx + k; j++) {
//             sum += arr[j];
//         }
//         result.push(sum / k);
//     }
//     return result;
// }

//Sliding window approach
//Time Complexity -> O(1) | Space -> O(N)
function subArrayAverage(k, arr) {
    const average = [];
    let currentSum = 0.0;
    let windowStart = 0;

    for (let windowEnd = 0; windowEnd < arr.length; windowEnd++) {
        currentSum += arr[windowEnd];
        if (windowEnd >= k - 1) {
            average.push(currentSum / k);
            currentSum -= arr[windowStart];
            windowStart++;
        }
    }
    return average;
}

console.log(subArrayAverage(5, [1, 3, 2, 6, -1, 4, 1, 8, 2]));
