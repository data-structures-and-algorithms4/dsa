// Given an integer array nums, find the contiguous subarray (containing at least one number)
// which has the largest sum and return its sum.
// A subarray is a contiguous part of an array.
var maxSubArray = function (nums) {
    let currentSum = 0;
    let largestSum = 0;

    for (let idx of nums) {
        currentSum += idx;
        console.log({ currentSum: currentSum });
        if (currentSum < 0) {
            currentSum = 0;
        }
        largestSum = Math.max(currentSum, largestSum);
    }
    return largestSum;
};

// console.log(maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]));
console.log(maxSubArray([5, 4, -1, 7, 8]));
