// Given a string s that represents a DNA sequence, and a number k, return all the
// contiguous sequences (substrings) of length k that occur more than once in the string.
// The order of the returned subsequences does not matter. If no repeated subsequence is found,
// the function should return an empty array.

//Solution
const findRepeatedSequence = (s, k) => {
    let windowSize = k;

    if (s.length <= windowSize) {
        return "string is too short";
    } else {
        return "string is long enough";
    }
};

console.log(findRepeatedSequence("AAAAACCCCCAAAAACCCCCC", 8));
