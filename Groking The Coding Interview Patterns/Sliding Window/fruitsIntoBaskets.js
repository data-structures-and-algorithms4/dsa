// You are visiting a farm to collect fruits. The farm has a single row of fruit trees. You will be given two baskets,
// and your goal is to pick as many fruits as possible to be placed in the given baskets.

// You will be given an array of characters where each character represents a fruit tree. The farm has following restrictions:

// Each basket can have only one type of fruit. There is no limit to how many fruit a basket can hold.
// You can start with any tree, but you can’t skip a tree once you have started.
// You will pick exactly one fruit from every tree until you cannot, i.e., you will stop when you have to pick from a third fruit type.
// Write a function to return the maximum number of fruits in both baskets.
//SOLUTION
// This problem follows the Sliding Window pattern and is quite similar to Longest Substring with K Distinct Characters.
// In this problem, we need to find the length of the longest subarray with no more than two distinct characters (or fruit types!).
// This transforms the current problem into Longest Substring with K Distinct Characters where K=2.
//
//Time Complexity O(N) The outer for loop runs for all characters, and the inner while loop processes each
//  character only once; therefore, the time complexity of the algorithm will be O(N+N),
// which is asymptotically equivalent to O(N) O(N).
// | Space O(K) where k is the number of characters in the hashmap

function fruits_into_baskets(fruits) {
    let prevFruit = 0,
        maxLength = 0,
        fruitFreq = {};

    //loop through the fruits to find how many we can store in the baskets
    for (let windowEnd = 0; windowEnd < fruits.length; windowEnd++) {
        const currentFruit = fruits[windowEnd];
        if (!(currentFruit in fruitFreq)) {
            fruitFreq[currentFruit] = 0;
        }
        fruitFreq[currentFruit] += 1;

        // check if we are storing maximum fruits of the same type
        while (Object.keys(fruitFreq).length > 2) {
            let tempFruit = fruits[prevFruit];
            fruitFreq[tempFruit] -= 1;
            if (fruitFreq[tempFruit] === 0) {
                delete fruitFreq[tempFruit];
            }
            prevFruit += 1;
        }
        maxLength = Math.max(maxLength, windowEnd - prevFruit + 1);
    }
    return maxLength;
}

console.log(fruits_into_baskets([1, 2, 6, 2, 2, 1, 3]));
