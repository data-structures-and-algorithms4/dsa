//Time Complexity
// PUT()The average-case complexity is O(1), while the worst-case complexity is O(n).
// GET() The average-case complexity is O(1), while the worst-case complexity is O(n).
// REMOVE() The average-case complexity is O(1), while the worst-case complexity is O(n).
//
//Space Complexity
// The space required for this data structure is the sum of the size of the hash map, m, and the number of existing key-value pairs, n. So, the space complexity would be O(m+n).
import Bucket from "./Bucket.mjs";

class HashMap {
    constructor(keySpace) {
        this.keySpace = keySpace;
        this.buckets = [];

        for (let i = 0; i < this.keySpace; i++) {
            this.buckets.push(new Bucket());
        }
    }

    put(key, value) {
        if (key == null || value == null) return -1;
        let hashKey = key % this.keySpace;
        this.buckets[hashKey].update(key, value);

        console.log(this.buckets);
    }

    get(key) {
        if (key === null) return -1;
        let hashKey = key % this.keySpace;
        return this.buckets[hashKey].get(key);
    }

    remove(key) {
        if (key === null) return -1;
        let hashKey = key % this.keySpace;
        return this.buckets[hashKey].remove(key);
    }
}

let h = new HashMap(11);

h.put(2, 5);
h.put(2, 7);
h.put(2, 8);
h.put(1, 6);
h.put(3, 8);
h.put(7, 4);
console.log(h.get(7));
console.log(h.remove(1));
console.log(h.remove(7));
console.log(h.remove(3));
console.log(h.remove(2));
h.put(4, 8);

// Run this file as such  node --experimental-modules HashMap.mjs
