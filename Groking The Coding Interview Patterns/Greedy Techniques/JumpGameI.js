// In a single-player jump game, the player starts at one end of a series of squares, with the goal of reaching the last square.

// At each turn, the player can take up to steps towards the last square, where s is the value of the current square.

// For example, if the value of the current square is 3, the player can take either 3 steps, or 2 steps, or 1
// step in the direction of the last square. The player cannot move in the opposite direction, that is, away from the last square.

// You have been tasked with writing a function to validate whether a player can win a given game or not.
// You’ve been provided with the nums integer array, representing the series of squares. The player starts at the first index and, following the rules of the game, tries to reach the last index.
// If the player can reach the last index, your function returns TRUE; otherwise, it returns FALSE.
//****************************************************************************** */
//Time Complexity - Since there are n elements in the array and we only visit each element once, the time complexity is O(n).
//Space Complexity - We don't use any additional data structures, so the space complexity is O(1)
//
//****************************************************************************** */
//Solution
const jumpGame = (nums) => {
    let targetIdx = nums.length - 1;

    //Start to iterate from the end of the array but on the last but one element
    for (let i = nums.length - 2; i >= 0; i--) {
        console.log(`Current index : ${i}`);
        //CHeck if the target index can be reached from the prev/preceding element value
        if (targetIdx === i) {
            console.log("Target is equal to current element");
        } else if (nums[i] >= targetIdx - i) {
            console.log(`Target : ${targetIdx} <= ${i} + ${nums[i]}`);
            targetIdx = i;
        } else {
            console.log(
                "Cannot reach target from here, moving to next element"
            );
        }
    }
    if (targetIdx === 0) return true;
    return false;
};

console.log(jumpGame([4, 0, 3, 0, 0, 1]));
