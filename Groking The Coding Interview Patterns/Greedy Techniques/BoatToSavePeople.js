//
// You’re given an array, people, where people[i] is the weight of the i^{th} person, and an infinite number of boats,
// where each boat can carry a maximum weight, limit. Each boat carries, at most, two people at the same time.
// This is provided that the sum of the weight of those people is under or equal to the weight limit.
// You need to return the minimum number of boats to carry every person in the array.
//********************************************************/
//Time Complexity The time complexity for the solution is O(n+n log n),
// despite us traversing only once. We use sorting which requires O(n log n).
//
//Space Complexity When we sort people array, we use extra space. The sorting algorithm’s space complexity is determined by the sorting method employed and differs from language to language.
// In Javascript, it takes O(n) , thus our space complexity for the solution is O(n).
//****************************************************** */

const rescueBoats = (people, limit) => {
    //Sort the people out so we can have the light and heavy people
    people.sort((a, b) => {
        return a, b;
    });

    let ships = 0;
    let lightest = 0;
    let heaviest = people.length - 1;

    //Check if 2 people are able to get on the boat, increment lightest else decrement heaviest
    //And increment ships used
    while (lightest <= heaviest) {
        ships += 1;

        //Check if lightest + heaviest is less than or equal to limit, decrease lightest
        if (people[lightest] + people[heaviest] <= limit) lightest++;

        //Else decrease heaviest
        heaviest--;
    }
    return ships;
};
