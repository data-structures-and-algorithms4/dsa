// Given an m×n 2-D grid of characters, we have to find a specific word in the grid by combining the adjacent characters.
// Assume that only up, down, right, and left neighbors are considered adjacent.
