// Given a chessboard of size n \times n×n, determine how many ways n queens can be placed on the board, such that no two queens attack each other.
//A queen can move horizontally, vertically, and diagonally on a chessboard.
// One queen can be attacked by another queen if both share the same row, column, or diagonal.
//
