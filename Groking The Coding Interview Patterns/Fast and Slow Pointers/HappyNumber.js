// Write an algorithm to determine if a number n is happy.

// A happy number is a number defined by the following process:

// *Starting with any positive integer, replace the number by the sum of the squares of its digits.
// *Repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1.
// *Those numbers for which this process ends in 1 are happy.
//
// Return TRUE if n is a happy number, and FALSE if not.
//The time complexity for the solution shown above is O(\log n). If no cycle exists, the fast pointer will reach 1
// and the slow pointer will reach halfway to 1. However, since there were two pointers instead of one,
//  we know that in the worst-case scenario, the cost was O(2 X log n).
//The memory(Space) complexity for the solution shown above is O(1) as we don't need any extra space because we are only manipulating pointers.
//
//Solution
function happyNumber(n) {
    //Helper function to get the sum of each digits squared
    function sumDigits(number) {
        let totalSum = 0;

        while (number > 0) {
            let digit = number % 10,
                temp = Math.floor(number / 10);
            number = temp;
            totalSum += digit ** 2;
        }
        return totalSum;
    }

    //Defining fast and slow pointers
    let slowPointer = n,
        fastPointer = sumDigits(n);

    //When fast pointer has not met slow pointer & fast pointer not equal to 1, call fast pointer twice and slow pointer once
    //This is to speed the process to make sure both pointers meet or fast pointer reaches 1
    while (fastPointer !== 1 && slowPointer !== fastPointer) {
        slowPointer = sumDigits(slowPointer);
        fastPointer = sumDigits(sumDigits(fastPointer));
    }
    //If fast pointer reaches 1, then terminate the program
    if (fastPointer == 1) {
        return true;
    }
    return false;
}

console.log(happyNumber(2147483646));
