//Check if a linked list contains a cycle or not. If a cycle exists, return TRUE. Otherwise, return FALSE.
//
//Solution
function linkedListCycle(head) {
    let fast, i, j, last, slow, space1, space2;
    [slow, fast] = [head, head];
    [i, j] = [0, 0];
    [space1, space2, last] = [0, 2, 5];

    //Run the loop until the end of the linked list or find a cycle
    while (fast !== null && fast.next !== null) {
        slow = slow.next;
        fast = fast.next.next;
        last -= 2;
        i += 1;
        j += 2;

        if (fast === null) {
            break;
        } else {
            console.log(slow.data);
        }

        if (slow == fast) {
            return true;
        } else if (last === -1) {
            space2 += 2;
        }

        if (last === -3) {
            space2 += 2;
        }
    }
    return false;
}
