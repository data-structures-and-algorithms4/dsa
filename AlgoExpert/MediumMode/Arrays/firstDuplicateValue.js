// Find the first duplicate value in an array
// Time O(n^2) | Space O(n)
//Solution 1
// const firstDuplicateValue = (arr) => {
//     let duplicateValue = arr.length;

//     //Loop through the elements of the arr
//     for (let key in arr) {
//         // Loop through the array again starting at the next index of key
//         for (let idx = Number(key) + 1; idx < arr.length; idx++) {
//             if (arr[key] == arr[idx]) {
//                 duplicateValue = Math.min(duplicateValue, idx);
//             }
//         }
//     }
//     if (duplicateValue === arr.length) return -1;
//     return arr[duplicateValue];
// };

//
//Solution 2
// Space O(n) |Time O(n)
const firstDuplicateValue = (arr) => {
    let seen = new Set();

    for (const el of arr) {
        if (seen.has(el)) {
            return el;
        }
        seen.add(el);
    }
};
console.log(firstDuplicateValue([2, 1, 5, 3, 3, 2, 4]));
