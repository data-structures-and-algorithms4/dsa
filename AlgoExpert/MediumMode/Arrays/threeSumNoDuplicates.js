// Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that
//  i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.
// Notice that the solution set must not contain duplicate triplets.

const threeNumberSum = (array) => {
    //first sort out the array in ascending order
    array.sort((a, b) => a - b);
    let triplets = [];

    //loop through the array to get your index, lef and right pointer and sum all to get your sum
    for (let i = 0; i < array.length; i++) {
        let left = i + 1;
        let right = array.length - 1;
        if (array[i] === array[i - 1]) {
            console.log(i, array[i], array[i - 1]);
            continue;
        }
        while (left < right) {
            let currentSum = array[i] + array[left] + array[right];
            if (currentSum === 0) {
                triplets.push([array[i], array[left], array[right]]);
                while (left < right && array[left] === array[left + 1]) left++;
                while (left < right && array[right] === array[right - 1])
                    right--;
                left++;
                right--;
            } else if (currentSum < 0) {
                left++;
            } else if (currentSum > 0) {
                right--;
            }
        }
    }
    return triplets;
};

console.log(threeNumberSum([-1, 0, 1, 2, -1, -4], 0));
