//Move element to the end of the array e.g [2,1,2,2,2,3,4,2] and move all two's to the end of the array
// Time O(n) | Space O(1) - as we are just changing positions of the arrays which is a constant operation
//Solution
//
const moveElementsToEnd = (arr, toMove) => {
    //declare your beginning and end pointers
    let start = 0;
    let end = arr.length - 1;

    //check if the start pointer does not overlap with the end pointer
    while (start < end) {
        //check the end pointer always points to an element not equal to (toMove) element.
        //Check to ensure the end pointer doesn't again overlap with the start pointer else will cause a bug
        while (start < end && arr[end] === toMove) {
            end -= 1;
        }
        //Check if the start pointer equals (toMove) element, then swap the values
        if (arr[start] === toMove) {
        }
        [arr[start], arr[end]] = [arr[end], arr[start]];
        start += 1;
    }
    return arr;
};

console.log(moveElementsToEnd([2, 1, 2, 2, 2, 3, 4, 2], 2));
