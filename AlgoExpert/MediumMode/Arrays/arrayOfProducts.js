//Find the product of elements in an array by multiplying the elements excluding the c
//current element itself
//E.g [5,1,4,2] -> [8,40,10,20]
///
// Solution 1
//Time O(n^2) | Space O(n)
// const arrayOfProduct = (arr) => {
//     let finalProduct = new Array(arr.length).fill(1);

//     for (const key in arr) {
//         let runningProduct = 1;
//         for (const idx in arr) {
//             if (idx !== key) runningProduct *= arr[idx];
//         }
//         finalProduct[key] = runningProduct;
//     }
//     return finalProduct;
// };

//
//Solution 2 Time O(n) | Space O(n)
const arrayOfProduct = (arr) => {
    let products = new Array(arr.length).fill(1);

    //Loop through array and find the running products to the left of the current element
    let leftRunningProduct = 1;
    for (const key in arr) {
        products[key] = leftRunningProduct;
        leftRunningProduct *= arr[key];
    }
    //Find the running products to the right of the current element
    // And multiply by the element at it's position in the products array
    //Do it in a reverse since we are finding the right products
    let rightRunningProduct = 1;
    for (let key = arr.length - 1; key >= 0; key--) {
        products[key] *= rightRunningProduct;
        rightRunningProduct *= arr[key];
    }

    return products;
};

console.log(arrayOfProduct([5, 1, 4, 2]));
