// Smallest Difference
//Find the closest pair that have the smallest difference in them. In this example ,
// the closest pair will be [28,27] since they have a small size of (2)
// Time O(n log(n) + m log(m)) | Space O(1) - since we are not storing anything that depends on the length of the input
//Solution
const smallestDifference = (arrayOne, arrayTwo) => {
    //Either choose to sort the arrays or not
    arrayOne.sort((a, b) => a - b);
    arrayTwo.sort((a, b) => a - b);
    let idxOne = 0;
    let idxTwo = 0;
    smallestPair = [];
    let smallest = Infinity;
    let currentDiff = 0;

    //loop through both arrays to check the smallest difference
    while (idxOne < arrayOne.length && idxTwo < arrayOne.length) {
        let firstNum = arrayOne[idxOne];
        let secondNum = arrayTwo[idxTwo];

        //check if first Number is less than second number? then move first number's pointer by one
        if (firstNum < secondNum) {
            currentDiff = secondNum - firstNum;
            idxOne += 1;
        } else if (secondNum < firstNum) {
            currentDiff = firstNum - secondNum;
            idxTwo += 1;
        } else {
            return [firstNum, secondNum];
        }

        if (smallest > currentDiff) {
            smallest = currentDiff;
            smallestPair = [firstNum, secondNum];
        }
    }
    return smallestPair;
};

console.log(
    smallestDifference([-1, 5, 10, 20, 28, 3], [27, 24, 134, 135, 15, 17])
);
