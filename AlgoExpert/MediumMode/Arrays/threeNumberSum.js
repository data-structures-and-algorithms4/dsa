// Three Sum - Medium Question
// Time O(n^2) | Space O(n)

const threeNumberSum = (array, target) => {
    //first sort out the array in ascending order
    array.sort((a, b) => a - b);
    let triplets = [];

    //loop through the array to get your index, lef and right pointer and sum all to get your sum
    for (let i = 0; i < array.length; i++) {
        let left = i + 1;
        let right = array.length - 1;

        while (left < right) {
            let currentSum = array[i] + array[left] + array[right];
            if (currentSum === target) {
                triplets.push([array[i], array[left], array[right]]);
                left++;
                right--;
            } else if (currentSum < target) {
                left++;
            } else if (currentSum > target) {
                right--;
            }
        }
    }
    return triplets;
};

// console.log(threeNumberSum([12, 3, 1, 2, -6, 5, -8, 6], 0));
console.log(threeNumberSum([-1, 0, 1, 2, -1, -4], 0));
