// Find the longest peak in an array
//Space O(1) | Time O(n)
//
// SOlution

const longestPeak = (array) => {
    let longestPeakLength = 0;
    let idx = 1;

    while (idx < array.length - 1) {
        let isPeak = idx > array[idx - 1] && idx > array[idx + 1];
        if (!isPeak) {
            idx++;
            continue;
        }

        //When peak is found, find length of peak by traversing leftward and rightward
        //Traverse left of the peak
        let leftIdx = idx - 2;
        while (leftIdx >= 0 && array[leftIdx] < array[leftIdx + 1]) {
            leftIdx--;
        }

        //Traverse right of the peak
        let rightIdx = idx + 2;
        while (
            rightIdx <= array.length &&
            array[rightIdx] < array[rightIdx - 1]
        )
            rightIdx++;

        //Check the length of the peak
        const currentPeakLength = rightIdx - leftIdx + 1;
        //Update the current peak length
        longestPeakLength = Math.max(longestPeakLength, currentPeakLength);
        idx = rightIdx;
    }
    return longestPeakLength;
};

console.log(longestPeak([1, 2, 3, 3, 4, 0, 10, 6, 5, -1, 0, 2, 3, 1]));
