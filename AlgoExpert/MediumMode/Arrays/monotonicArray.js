//Monotomic Array
// An array is monotomic if it's non-decreasing or non-increasing
//
//Solution 1 ------ Time O(n) | Space O(1)
// const isMonotomic = (array) => {
//     //check if the elements in the array are just two or less
//     if (array.length <= 2) return true;

//     //Check the direction of the array if its increasing or decreasing
//     let direction = array[1] - array[0];
//     for (let i = 2; i < array.length; i++) {
//         //if the direction is flat or equal to 0, then change the direction
//         if (direction === 0) direction = array[i] - array[i - 1];
//         continue;

//         //Helper function to check the direction if the array elements and return true or false
//         if (breaksDirection(direction, array[i - 1], array[i])) return false;
//     }
//     return true;

//     //Helper function to check if the loop breaks the direction or not
//     function breaksDirection(direction, previousNumber, currentNumber) {
//         const difference = currentNumber - previousNumber;
//         if (direction > 0) {
//             return difference < 0;
//         }
//         return difference > 0;
//     }
// };

//Solution 2 ------ Time O(n) | Space O(1)
const isMonotomic2 = (arr) => {
    let isNonIncreasing = true;
    let isNonDecreasing = true;
    for (let idx = 1; idx < arr.length; idx++) {
        if (arr[idx] < arr[idx - 1]) {
            isNonDecreasing = false;
        }
        if (arr[idx] > arr[idx - 1]) {
            isNonIncreasing = false;
        }
    }
    console.log({
        isNonDecreasing: isNonDecreasing,
        isNonIncreasing: isNonIncreasing,
    });

    return isNonDecreasing || isNonIncreasing;
};

console.log(isMonotomic2([-1, -5, -10, -1100, -1100, -1101, -1102, -90011]));
