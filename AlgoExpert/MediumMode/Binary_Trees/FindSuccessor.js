// FInd Successor in Binary Tree
class BinaryTree {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
        this.parent = null;
    }
}

//Solution 1 Time O(n) | Space O(n)
//This solution uses the inorder traversal to find all elements of the tree and find the
//successor of the "node"
const findSuccessor = (tree, node) => {
    let inOrderTraversalNodes = getInorderTraversalNodes(tree);

    for (const idx in inOrderTraversalNodes) {
        //check if element is not what we are looking for? then continue
        if (inOrderTraversalNodes[idx] !== node) continue;

        //check if the node is the last element in the "inOrderTraversalNodes" array?
        if (node === inOrderTraversalNodes[inOrderTraversalNodes.length - 1]) {
            return None;
        }

        //check if there is a successor in inOrderTraversalNodes
        if (node === inOrderTraversalNodes[idx])
            return inOrderTraversalNodes[idx + 1];
    }
};

//This get an inorder of a binary search tree
const getInorderTraversalNodes = (node, order = []) => {
    if (node === None) return order;

    getInorderTraversalNodes(node.left, order);
    order.push(node.value);
    getInorderTraversalNodes(node.right, order);
};

//Solution 2
//Time O(h) as "h" is the hight we have to traverse
// Space is O(1) as we are constantly storing currentNode in a single variable and not doing complex computations
const findSuccessorInBinaryTree = (node) => {
    //Check if there is a node.right present to find the left most node as it's successor
    if (node.right !== null) {
        return getLeftMostChild(node.right);
    }

    return getRightMostParent(node);
};

const getLeftMostChild = (node) => {
    currentNode = node;
    while (currentNode.left) {
        currentNode = currentNode.left;
    }
    return currentNode;
};

const getRightMostParent = (node) => {
    let currentNode = node;
    while (
        currentNode.parent !== null &&
        currentNode.parent.right === currentNode
    ) {
        currentNode = currentNode.parent;
    }
    return currentNode.parent;
};
