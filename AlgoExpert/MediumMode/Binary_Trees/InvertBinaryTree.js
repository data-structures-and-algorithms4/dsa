// Invert a Binary Tree
//
//Solution 1 Using bfs and swapping the nodes
// Time O(n) | Space O(n)
// const invertBinaryTree = (tree) => {
//     let queue = [tree];

//     while (queue.length) {
//         let currentNode = queue.shift(0);
//         if (currentNode === None) {
//             continue;
//         }

//         swapLeftAndRightNodes(currentNode);
//         queue.push(currentNode.left);
//         queue.push(currentNode.right);
//     }
// };
function swapLeftAndRightNodes(tree) {
    [tree.left, tree.right] = [tree.right, tree.left];
}

//Solution 2 Using Recursion
// Time O(n) | Space O(d) as "d" is the depth of the tree

const invertBinaryTree = (tree) => {
    if (tree === None) return;

    swapLeftAndRightNodes(tree);
    invertBinaryTree(tree.left);
    invertBinaryTree(tree.right);
};
