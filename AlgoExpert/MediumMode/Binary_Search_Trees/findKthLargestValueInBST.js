//Find kth largest value in BST
//Define BST class and insert function
//Solution 1 Time O(n) | Space O(n)
class BST {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
        this.size = 1;
    }

    insert(value) {
        if (value < this.value) {
            if (this.left === null) {
                this.left = new BST(value);
            } else {
                this.left.insert(value);
            }
        } else {
            if (this.right === null) {
                this.right = new BST(value);
            } else {
                this.right.insert(value);
            }
        }

        this.size++;
    }

    getSize() {
        return this.size;
    }

    inOrderTraversal(k) {
        let sortedNodeValues = [];

        //Create a function to traverse bst
        const traverse = (node) => {
            if (node === null) return;

            if (node.left) traverse(node.left);
            sortedNodeValues.push(node.value);
            if (node.right) traverse(node.right);
        };

        traverse(this.value);

        return sortedNodeValues;
    }
}

const bst = new BST(15);
bst.insert(3);
bst.insert(36);
bst.insert(2);
bst.insert(12);
bst.insert(28);
bst.insert(42);
console.log(bst.inOrderTraversal(2));
