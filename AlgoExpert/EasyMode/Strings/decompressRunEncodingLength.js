// We are given a list nums of integers representing a list compressed with run-length encoding.
// Consider each adjacent pair of elements [freq, val] = [nums[2*i], nums[2*i+1]] (with i >= 0).
// For each such pair, there are freq elements with value val concatenated in a sublist.
// Concatenate all the sublists from left to right to generate the decompressed list.
// Return the decompressed list.

var decompressRLElist = function (nums) {
    let freq = [];

    for (let windowEnd = 0; windowEnd < nums.length; windowEnd++) {
        let frequency = nums[windowEnd];
        let value = nums[windowEnd + 1];

        if ((windowEnd + 2) % 2 === 0) {
            console.log(value);
            for (let idx = 0; idx < frequency; idx++) {
                freq.push(value);
            }
        }
    }
    return freq;
};

console.log(decompressRLElist([1, 2, 3, 4]));
