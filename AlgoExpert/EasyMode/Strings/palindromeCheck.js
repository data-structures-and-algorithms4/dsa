// Check if a string is palindrome
// E.g abcdcba
//
//Solution 1
// Time -> O(n^2) | O(n)
// const palindromeCheck = (string) => {
//     let reversedString = "";
//     for (let i = string.length - 1; i >= 0; i--) {
//         reversedString += string[i];
//     }
//     return reversedString === string ? true : false;
// };

//Solution 2 using pointers to check the last and first elements then increment them to check the other elements
//Time O(n) | Space O(n)
const palindromeCheck = (string) => {
    let lastElement = string.length - 1;
    let first = 0;
    while (first < lastElement) {
        if (string[first] !== string[lastElement]) {
            return false;
        }
        lastElement--;
        first++;
    }
    return true;
};
console.log(palindromeCheck("raceacar"));
