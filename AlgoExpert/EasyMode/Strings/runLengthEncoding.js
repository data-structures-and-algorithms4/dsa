//Run Length Encoding
// Time -> O(n) | Space -> O(n)
//Solution
// Run-length encoding (RLE) is a form of lossless data compression in which runs of data (sequences in which the same data value occurs in many consecutive data elements)
// are stored as a single data value and count, rather than as the original run.
//
//currentRunLength should have a limit of 9 as this will not be confusing if e.g 13A -> does the 1 belong to 3 or does 3 belong to A???

// const runLengthEncoding = (string) => {
//     let encodedStringCharacters = [];
//     let currentRunLength = 1;

//     //Loop through the string starting at index 1 and compare the characters
//     for (let idx = 1; idx < string.length; idx++) {
//         currentChar = string[idx];
//         prevChar = string[idx - 1];

//         //Check if the currentChar not equal to prevChar or currentRunLength not equal to 9
//         if (currentChar != prevChar || currentRunLength == 9) {
//             encodedStringCharacters.push(currentRunLength.toString());
//             encodedStringCharacters.push(prevChar);
//             //Reset current to zero to accommodate the next character's runLength
//             currentRunLength = 0;
//         }
//         //Add 1 to currentRunLength to either increase the running characters or start from the current index + 1 char to compare with prev char
//         currentRunLength += 1;
//     }

//     //Edge case for the last character(s) since there won't be a next element to compare with as the loop has ended
//     encodedStringCharacters.push(currentRunLength.toString());
//     encodedStringCharacters.push(string[string.length - 1]);

//     //Return a string using the join method
//     return encodedStringCharacters.join("");
// };

//Using sliding window pattern
const runLengthEncoding = (string) => {
    let encodedStringCharacters = [];
    let currentRunLength = 1;

    for (let windowEnd = 0; windowEnd < string.length; windowEnd++) {
        if (string[windowEnd] === string[windowEnd + 1]) {
            currentRunLength++;
        } else {
            encodedStringCharacters.push(currentRunLength.toString());
            encodedStringCharacters.push(string[windowEnd]);
            currentRunLength = 1;
        }
    }
    return encodedStringCharacters.join("");
};

console.log(runLengthEncoding("a"));
