// Given a string s, find the first non-repeating character in it and return its index.
// If it does not exist, return -1.
//
//

//SOlution -> Time O(n) | Space O(1)
var firstUniqChar = function (s) {
    let charFreqMap = {};

    for (const el of s) {
        if (!(el in charFreqMap)) {
            charFreqMap[el] = 0;
        }
        charFreqMap[el] += 1;
    }

    for (let idx = 0; idx < s.length; idx++) {
        character = s[idx];
        if (charFreqMap[character] === 1) {
            return s[idx];
        }
    }
    return -1;
};

console.log(firstUniqChar("aabbs"));
