// Generate Document
//Given two strings, return true if the characters from the one string can be found in the other string
//
//Solution 1 - Time O(m * (m + n)) | Space O(1)

// const generateDocument = (characters, documents) => {
//     for (const el of characters) {
//         const charFreq = countFreqCharacters(el, characters);
//         const docFreq = countFreqCharacters(el, documents);

//         if (charFreq > docFreq) {
//             return false;
//         }
//     }
//     return true;

//     function countFreqCharacters(el, target) {
//         let frequency = 0;
//         for (const element of target) {
//             console.log(element);
//             if (el === element) {
//                 frequency += 1;
//             }
//         }
//         return frequency;
//     }
// };

//Solution 2 - Time O(c * (m + n)) | Space O(c) where c is the number of unique characters in the document
// const generateDocument = (characters, documents) => {
//     let alreadyCounted = new Set();

//     for (const el of characters) {
//         if (alreadyCounted.has(el)) {
//             continue;
//         }
//         const charFreq = countFreqCharacters(el, characters);
//         const docFreq = countFreqCharacters(el, documents);

//         if (charFreq > docFreq) {
//             return false;
//         }

//         alreadyCounted.add(el);
//     }
//     return true;

//     function countFreqCharacters(el, target) {
//         let frequency = 0;
//         for (const element of target) {
//             if (el === element) {
//                 frequency += 1;
//             }
//         }
//         return frequency;
//     }
// };

//Solution 3 -> O(n + m) Time | O(c) Space where c is the unique characters we keep track of in the object
const generateDocument = (characters, documents) => {
    let characterCount = {};

    for (const el of characters) {
        if (!(el in characterCount)) {
            characterCount[el] = 0;
        }
        characterCount[el] += 1;
    }

    for (const el of documents) {
        if (!(el in characterCount) || characterCount[el] === 0) {
            return false;
        }
        characterCount[el] -= 1;
    }
    return true;
};

console.log(generateDocument("cssap", "cas"));
