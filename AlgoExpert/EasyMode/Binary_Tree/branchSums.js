// Branch Sums - > BINARY TREE USING RECURSIVE APPROACH AND THE DEPTH FIRST SEARCH APPROACH TOO
class BinaryTree {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
    calculateBranchSum(node, runningSum, sums) {
        if (!node) return;

        const newRunningSum = runningSum + node.value;
        if (!node.left && !node.right) {
            sums.push(newRunningSum);
            return;
        }
        calculateBranchSum(node.left, runningSum, sums);
        calculateBranchSum(node.right, runningSum, sums);
    }

    // Space -> O(n) | Time -> O(n)
    branchSum(root) {
        let sums = [];
        calculateBranchSum(root, 0, sums);
        return sums;
    }
}

var binarySearchBranchSums = new BinaryTree([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
binarySearchBranchSums.branchSum();
