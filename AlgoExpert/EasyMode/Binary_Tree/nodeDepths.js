//Node Depth
// Find the depth(height) of a binary tree and populate the total sum of the depths.
//E.g binary_tree [1,2,3,4,5,6,7,8,9] -> total_depth = 16
//Iterative Approach - Time O(n) | Space O(h) where h is the height of the binary tree as the stack increases
//
//Solution - Iterative Approach
// const nodeDepths = (root) => {
//   let sumOfDepths = 0
//   let stack = [{"node": root, "depth": 0}]

//   while (stack.length > 0) {
//     let nodeInfo = stack.pop()
//     const {node,depth} = nodeInfo
//     if (node == none) continue;

//     sumOfDepths += depth
//     stack.push([{"node": node.left, "depth": depth + 1}])
//     stack.push([{"node": node.right, "depth": depth + 1}])
//   }
//   return sumOfDepths
// }

//Recursive Approach - Time O(n) | Space O(h)
const nodeDepths = (root, depth = 0) => {
    if (!root) return 0;
    return (
        depth +
        nodeDepths(root.left, depth + 1) +
        nodeDepths(root.right, depth + 1)
    );
};

console.log(nodeDepths([1, 2, 3, 4, 5, 6, 7, 8, 9]));
