// Class Photos
//Time -> O(n log n) since we are using sort | Space -> O(1) since we are compare the highest integer
// Sort both arrays and first check the tallest(highest) integer or student to know where
// to place the team or array the it falls in whether back or front
/////////// Solution with code //////////////
function classPhotos(redShirtHeights, blueShirtHeights) {
    redShirtHeights.sort((a, b) => b - a);
    blueShirtHeights.sort((a, b) => b - a);

    shirtColorInFirstRow =
        redShirtHeights[0] < blueShirtHeights[0] ? "RED" : "BLUE";

    for (const idx in redShirtHeights) {
        const redShirtHeight = redShirtHeights[idx];
        const blueShirtHeight = blueShirtHeights[idx];
        if (shirtColorInFirstRow == "RED") {
            if (redShirtHeight > blueShirtHeight) {
                return false;
            } else {
                if (blueShirtHeight < redShirtHeight) {
                    return false;
                }
            }
        }
    }
    return true;
}

console.log(classPhotos([7, 8, 1, 3, 4], [9, 6, 2, 4, 5]));
