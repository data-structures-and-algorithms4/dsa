const add = (nums) => {
    const sortedNums = nums.sort((a, b) => {
        const ab = a.toString() + b.toString();
        const ba = b.toString() + a.toString();
        return ba - ab;
    });

    const joined = sortedNums.join("");
    //converted the joined to a number using '+' operator
    if (+joined <= "0") {
        return "0";
    } else {
        return joined;
    }
};

console.log(add([0, 30]));
