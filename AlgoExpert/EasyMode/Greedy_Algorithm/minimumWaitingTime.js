// Minimum Waiting Time
// Time -> O(n log n) | Space -> O(1)
// Sort the array by ascending order to get the smallest elements first and largest elements last to make sure you are
// getting the minimum waiting time for each element
//
/////////// Solution with code //////////////
function minimumWaitingTime(queries) {
    queries.sort(function (a, b) {
        return a - b;
    });

    // let minimumTimeValue = 0;
    // for (const [idx, element] of queries.entries()) {
    //     const queriesLeft = queries.length - (idx + 1);
    //     minimumTimeValue += queriesLeft * element;
    // }
    // return minimumTimeValue;

    let minimumTimeValue = 0;
    let total = 0;
    for (const idx in queries) {
        if (idx <= queries.length && idx != 0) {
            minimumTimeValue += queries[idx - 1];
            console.log({ minimumTimeValue: minimumTimeValue });
            total += minimumTimeValue;
        }
    }
    return total;
}

// console.log(minimumWaitingTime([3, 2, 18, 2, 6, 4, 11, 6, 8]));
console.log(minimumWaitingTime([3, 2, 1, 2, 6]));
// console.log(minimumWaitingTime([2, 3, 4]));
