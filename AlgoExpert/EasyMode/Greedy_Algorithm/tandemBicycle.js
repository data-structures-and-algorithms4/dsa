//Given two sets of arrays and a boolean, Find the fastest speed possible among the arrays( can also be slowest)
// Solution
// First sort the arrays from ascending
// If fastest, reverse one of the from descending and the other from ascending and compare the two arrays by the the maximum int,
//  from one array against the minimum int from the other array
//If slowest, reverse all the arrays from descending order and compare them by the maximum number in one array and the maximum in the other array

/**
 *
 *
 * @param {*} redShirtSpeed
 * @param {*} blueShirtsSpeed
 * @param {*} fastest
 * @return {*}
 */
function tandemBicycle(redShirtSpeed, blueShirtsSpeed, fastest) {
    let totalSpeed = 0;
    redShirtSpeed.sort((a, b) => a - b);
    blueShirtsSpeed.sort((a, b) => a - b);
    console.log({redShirtSpeed: redShirtSpeed});

    if (!fastest) {
        redShirtSpeed.reverse()
    }
    console.log(redShirtSpeed);

    for (idx in redShirtSpeed) {
        rider1 = redShirtSpeed[idx];
        rider2 = blueShirtsSpeed[blueShirtsSpeed.length - idx - 1];
        totalSpeed += Math.max(rider1, rider2);
    }

    return totalSpeed;
}

console.log(tandemBicycle([5, 5, 3, 9, 2], [3, 6, 7, 2, 1], false));
