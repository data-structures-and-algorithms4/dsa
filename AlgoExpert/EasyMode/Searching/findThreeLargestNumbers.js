//FInd the Three largest Numbers
// Time O(n) | Space O(1)
//
//  SOLUTION /////////////
const threeLargestNumbers = function (array) {
    let largestNums = [null, null, null];
    for (const nums of array) {
        updateLargeNums(nums, largestNums);
    }
    return largestNums;

    function updateLargeNums(nums, largestNums) {
        if (largestNums[2] == null || nums > largestNums[2]) {
            shiftAndUpdate(largestNums, nums, 2);
        } else if (largestNums[1] == null || nums > largestNums[1]) {
            shiftAndUpdate(largestNums, nums, 1);
        } else if (largestNums[0] == null || nums > largestNums[0]) {
            shiftAndUpdate(largestNums, nums, 0);
        }
    }

    function shiftAndUpdate(array, nums, idx) {
        for (let index = 0; index < idx + 1; index++) {
            if (index === idx) {
                array[index] = nums;
            } else {
                array[index] = array[index + 1];
            }
        }
    }
};
// console.log(threeLargestNumbers[(141, 1, 17, -7, -17, -27, 18, 541, 8, 7, 7)]);
// console.log(threeLargestNumbers([141, 1, 17, -7, -17, -27, 18, 541, 8, 7, 7]));
