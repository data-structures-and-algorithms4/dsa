// Given an array of integers nums which is sorted in ascending order, and an integer target,
//  write a function to search target in nums. If target exists, then return its index. Otherwise, return -1.
// You must write an algorithm with O(log n) runtime complexity.
//Example 1
// Input: nums = [-1,0,3,5,9,12], target = 9
// Output: 4
// Explanation: 9 exists in nums and its index is 4
//
//
// Time O(n) | Space O(1)
// let search = function (nums, target) {
//     let counter = 0;
//     for (idx of nums) {
//         if (idx === target) {
//             break;
//         }
//         counter++;
//     }
//     if (counter == nums.length) {
//         return -1;
//     } else {
//         return counter;
//     }
// };

//Using another approach
//Time O(logN) | Space O(1)
let search = function (array, target) {
    return binarySearchHelper(array, target, 0, array.length - 1);

    function binarySearchHelper(array, target, left, right) {
        if (left > right) {
            return -1;
        }
        const middle = Math.floor((left + right) / 2);
        let potentialMatch = array[middle];
        if (target === potentialMatch) {
            return middle;
        } else if (target < potentialMatch) {
            right = middle - 1;
            return binarySearchHelper(array, target, left, right);
        } else if (target > potentialMatch) {
            left = middle + 1;
            return binarySearchHelper(array, target, left, right);
        }
    }
};

console.log(search([-1, 0, 3, 5, 9, 12], 3));
