// e're tasked with writing a function that takes in a "special" array and returns the product sum.
// The special array is non-empty and contains integers or other arrays.
// We can get the product sum of an array by adding all of its elements together and then multiplying by the level of depth of the array.
// The depth is determined by how nested the array is.
// Ex. The depth of [] is 1. The depth of the inner array in [[]] is 2. The depth of the innermost array in [[[]]] is 3.
// Eg . [5, 2 [-7, 1], 3, [6, [-13, 8], 4]]
//Solution
function productSum(arr, multiplier = 1) {
    let sum = 0;

    for (const idx of arr) {
        //check if the next element is a special array(e.g it has another array inside)
        if (Array.isArray(idx)) {
            sum += productSum(idx, multiplier + 1);
        } else {
            sum += idx;
        }
    }
    return sum * multiplier;
}

console.log(productSum([5, 2, [7, -1], 3, [6, [-13, 8], 4]]));
