// The Fibonacci numbers, commonly denoted F(n) form a sequence, called the Fibonacci sequence,
//  such that each number is the sum of the two preceding ones, starting from 0 and 1. That is,
// F(0) = 0, F(1) = 1
// F(n) = F(n - 1) + F(n - 2), for n > 1.
// Given n, calculate F(n).
//Example
// Input: n = 2
// Output: 1
// Explanation: F(2) = F(1) + F(0) = 1 + 0 = 1 .
// NAIVE APPROACH ////// -> O(2^n) time | O(n) Space
// function fib(n) {
//     if (n === 2) {
//         return 1;
//     } else if (n === 1) {
//         return 0;
//     }

//     return fib(n - 1) + fib(n - 2);
// }

//Memoize Approach
//Time O(n) | Space O(n)
// var fib = function (n, memoize = { 1: 1, 2: 1, 0: 0 }) {
//     if (n in memoize) {
//         return memoize[n];
//     } else {
//         memoize[n] = fib(n - 1, memoize) + fib(n - 2, memoize);
//         return memoize[n];
//     }
// };

//Iterative Approach -> Space O(n) | Time O(1)
function fib(n) {
    let currTwo = [0, 1];
    let counter = 2;

    while (counter <= n) {
        const sum = currTwo[0] + currTwo[1];
        currTwo[0] = currTwo[1];
        currTwo[1] = sum;
        counter++;
    }

    return n >= 1 ? currTwo[1] : currTwo[0];
}

console.log(fib(4));
