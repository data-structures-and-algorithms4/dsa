//What is recursion?
//It is a function that calls itself
function countdown(n) {
    for (let index = n; index > 0; index--) {
        console.log(index);
    }
    console.log("done");
}

function countdownRecursive(n) {
    if (n <= 0) {
        console.log("hurray");
        return;
    }
    console.log(n);
    countdownRecursive(n - 1);
}

//Normal sum of a range using iteration
function sumRange(n) {
    let total = 0;
    for (let index = n; index > 0; index--) {
        total += index;
    }
    return total;
}

//Using recursion to find the sum of a range
function sumOfRangeWithRecursion(n, total = 0) {
    if (n <= 0) {
        return total;
    }
    return sumOfRangeWithRecursion(n - 1, (total += n));
}

//Using recursion to print all children
function printChildrenNameRecursive(t) {
    if (t.children.length === 0) {
        return;
    }

    t.children.forEach((child) => {
        console.log(child.name);
        printChildrenNameRecursive(child);
    });
}

const tree = {
    name: "John",
    children: [
        {
            name: "Jim",
            children: [
                { name: "Ben", children: [] },
                { name: "Frank", children: [{ name: "Kessie", children: [] }] },
            ],
        },
        {
            name: "Zoe",
            children: [
                { name: "Kyle", children: [] },
                { name: "Sophia", children: [{ name: "Ruth", children: [] }] },
            ],
        },
    ],
};

// countdownRecursive(10);
// console.log(sumOfRangeWithRecursion(3));
printChildrenNameRecursive(tree);
