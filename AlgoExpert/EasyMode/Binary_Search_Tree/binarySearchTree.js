//Understand binary search tree
class Node {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
}

class BST {
    constructor(value) {
        this.root = new Node(value);
        this.size = 1;
    }

    //Get the size of the BST
    getSize() {
        return this.size;
    }

    //Insert a value
    insert(value) {
        this.size++;

        let newNode = new Node(value);

        const searchTree = (node) => {
            //check if the value is greater or less than the root node
            if (value < node.value) {
                //check if another node on the left side exist. If not insert
                if (!node.left) {
                    node.left = newNode;
                } else {
                    searchTree(node.left);
                }
                //If the value is greater than node.right
            } else if (value > node.value) {
                //check if another node on the right side exist. If not insert
                if (!node.right) {
                    node.right = newNode;
                } else {
                    searchTree(node.right);
                }
            }
        };

        searchTree(this.root);
    }

    //Get the minimum value
    getMin() {
        let currentNode = this.root;

        //Traverse the left nodes until no children
        while (currentNode.left) {
            currentNode = currentNode.left;
        }
        return currentNode.value;
    }

    //Get the maximum value
    getMax() {
        let currentNode = this.root;

        //Traverse the right nodes until no children
        while (currentNode.right) {
            currentNode = currentNode.right;
        }
        return currentNode.value;
    }

    //Check if BST contains
    contains(value) {
        let currentNode = this.root;

        while (currentNode) {
            if (value === currentNode.value) {
                return true;
            }

            if (value < currentNode.value) {
                currentNode = currentNode.left;
            } else {
                currentNode = currentNode.right;
            }
        }
        return false;
    }

    //Some algorithms on BST

    //Dept-First-Search - looks at branch by branch

    //in-order
    //left,root,right
    dfsInOrder() {
        let results = [];

        const traverse = (node) => {
            //If left child exist go left again
            if (node.left) traverse(node.left);
            //Capture the root node value
            results.push(node.value);
            //If right child exist go right
            if (node.right) traverse(node.right);
        };

        traverse(this.root);

        return results;
    }

    //pre-order
    //root,left,right
    dfsPreOrder() {
        let results = [];

        const traverse = (node) => {
            //Capture the root node value
            results.push(node.value);
            //If left child exist go left again
            if (node.left) traverse(node.left);
            //If right child exist go right
            if (node.right) traverse(node.right);
        };
        traverse(this.root);
        return results;
    }

    //post-order
    //left,right,root
    dfsPostOrder() {
        let results = [];

        const traverse = (node) => {
            //If left child exist go left again
            if (node.left) traverse(node.left);
            //If right child exist go right
            if (node.right) traverse(node.right);
            //Capture the root node value
            results.push(node.value);
        };
        traverse(this.root);
        return results;
    }

    //Breadth First Search - looks at level by level down
    //Use a queue!
    bfs() {
        let results = [];
        let queue = [];

        queue.push(this.root);

        while (queue.length) {
            let currentNode = queue.shift();

            results.push(currentNode.value);

            if (currentNode.left) queue.push(currentNode.left);

            if (currentNode.right) queue.push(currentNode.right);
        }
        return results;
    }
}

const bst = new BST(15);
bst.insert(3);
bst.insert(36);
bst.insert(2);
bst.insert(12);
bst.insert(28);
bst.insert(42);
// bst.insert(39);
// bst.insert(1);
// bst.insert(4);
// bst.insert(9);
// bst.insert(13);
// bst.insert(25);
// bst.insert(30);
// bst.insert(35);

// console.log(bst);
// console.log(bst.size);
// console.log(bst.getSize());
// console.log(bst.getMin());
// console.log(bst.getMax());
// console.log(bst.contains(2));
// console.log(bst.contains(1));
// console.log(bst.contains(15));
// console.log(bst.dfsInOrder());
// console.log(bst.dfsPreOrder());
// console.log(bst.dfsPostOrder());
console.log(bst.bfs());
