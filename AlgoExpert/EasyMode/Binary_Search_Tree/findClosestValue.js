// Average : Time = O(log(N)) | Space O(log(N)) -> In recursive mode
// Recursive Method
function findClosestValueInBst(tree, target) {
    return findClosestValueInBstHelper(tree, target, Infinity);

    function findClosestValueInBstHelper(tree, target, closest) {
        if (tree === none) return closest;
        // Check for the closest value.. if the current closet is bigger than the current value,
        //  then replace the current closest with the current value
        if (Math.abs(target - closest) > Math.abs(target - tree.value))
            closest = tree.value;

        //Check if target is smaller than the current node's value , then traverse left
        if (target < tree.value) {
            return findClosestValueInBst(tree.left, target, closest);
        }
        //Check if target is bigger than the current node's value , then traverse right
        else if (target > tree.value) {
            return findClosestValueInBst(tree.right, target, closest);
        } else {
            //Check if the node has no nodes to traverse , then return the closest
            return closest;
        }
    }
}

// Iterative Method
// Average : Time = O(log(N)) | Space O(1) -> In iterative mode
function findClosestValueInBst(tree, target) {
    return findClosestValueInBstHelper(tree, target, Infinity);

    function findClosestValueInBstHelper(tree, target, closest) {
        currentNode = tree;
        while (currentNode) {
            if (
                Math.abs(target - closest) >
                Math.abs(target - currentNode.value)
            )
                closest = currentNode.value;
            //Check if target is smaller than the current node's value , then traverse left
            if (target < currentNode.value) {
                currentNode = currentNode.left;
            }
            //Check if target is bigger than the current node's value , then traverse right
            else if (target > currentNode.value) {
                currentNode = currentNode.right;
            } else {
                //Check if the node has no nodes to traverse , then return the closest
                break;
            }
        }
        return closest;
    }
}
