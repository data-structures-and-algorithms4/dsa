//Bubble sort Implementation
// Space O(1) | Time O(n^2)
//Example [2,3,5,5,6,8,9]
//Solution
const bubbleSort = function (array) {
    let isSorted = false;
    while (!isSorted) {
        isSorted = true;
        for (let key = 0; key < array.length - 1; key++) {
            if (array[key] > array[key + 1]) {
                [array[key], array[key + 1]] = [array[key + 1], array[key]];
                isSorted = false;
            }
        }
    }
    return array;
};

console.log(bubbleSort([8, 6, 1, 3, 4, 5, 2]));
