//INsertion sort Implementation
// Space O(1) | Time O(n^2) where m is the length of our input array
//Example [2,3,5,5,6,8,9]
//Solution
//With insertion sort, you should always start from index[1], and compare the index[1] of the array to the index[0] and make a swap if it can be swapped.
//Example -> [3,1,5,7,8] : always start from index[1] and check with previous index if it can be swapped then increment index[1] by adding 1 and check with the last 2 elements if it can be swapped
const insertionSort = function (array) {
    for (let idx = 1; idx < array.length; idx++) {
        let j = idx;
        while (j > 0 && array[j] < array[j - 1]) {
            [array[j], array[j - 1]] = [array[j - 1], array[j]];
            j -= 1;
        }
    }
    return array;
};
console.log(insertionSort([5, 2, 8, 9, 5, 6, 3]));
