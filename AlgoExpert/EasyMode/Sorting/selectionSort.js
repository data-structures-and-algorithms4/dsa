//Selection sort Implementation
// Space O(1) | Time O(n^2) where m is the length of our input array
//Example [2,3,5,5,6,8,9]
//Solution
//Start from index[0] and iterate through the array for the minimum number and swap it.
// Then do same thing for incremental indexes and swap them with the minimum integers
const selectionSort = (array) => {
    let currentIdx = 0;
    while (currentIdx < array.length - 1) {
        let smallestIdx = currentIdx;
        for (let i = currentIdx + 1; i < array.length; i++) {
            if (array[smallestIdx] > array[i]) {
                smallestIdx = i;
            }
        }
        [array[currentIdx],array[smallestIdx]]  = [array[smallestIdx], array[currentIdx]];
        currentIdx++;
    }
    return array;
};

console.log(selectionSort([1,5, 2, 8, 9, 5, 6, 3]));
