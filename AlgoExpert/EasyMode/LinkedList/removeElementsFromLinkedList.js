// Given the head of a linked list and an integer val,
// remove all the nodes of the linked list that has Node.val == val, and return the new head.
//
//
// LEETCODE 203 //
//Always create a dummy node to point to the first node when you inserting or deleting from a node
//
function ListNode(val, next) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
}
var removeElements = function (head, val) {
    let dummy = new ListNode(-1);
    dummy.next = head;
    let prev = dummy;
    let cur = head;

    while (cur) {
        if (cur.val === val) {
            prev.next = cur.next;
            cur = cur.next;
        } else {
            prev = cur;
            cur = cur.next;
        }
    }
    return dummy.next;
};
