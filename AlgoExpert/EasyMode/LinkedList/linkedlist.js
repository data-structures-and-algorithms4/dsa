// How to create a linked list and insert,delete node

class Node {
    constructor(data, next = null) {
        this.data = data;
        this.next = next;
    }
}

class LinkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }

    //Insert first node
    insertFirst(data) {
        this.head = new Node(data, this.head);
        this.size++;
    }

    //Insert last node
    insertLast(data) {
        let node = new Node(data);
        let current;

        //If empty, make new head
        if (!this.head) {
            this.head = node;
        } else {
            current = this.head;

            while (current.next) {
                current = current.next;
            }
            current.next = node;
        }
        this.size++;
    }

    //Insert at Index
    insertAt(data, index) {
        //if it is the only node, set as head - edge case
        if (index === 0) {
            this.head = new Node(data, this.head);
            return;
        }

        //IF index out of out bounds/range - edge case
        if (index > 0 && index > this.size) {
            return;
        }

        const node = new Node(data);
        let current, previous;

        //set current to first
        current = this.head;
        let count = 0;
        while (count < index) {
            previous = current; //node before index
            count++;
            current = current.next; //node after index
        }

        node.next = current;
        previous.next = node;

        this.size++;
    }

    //Get at Index
    getAt(index) {
        let current = this.head;
        let count = 0;

        while (current) {
            if (count == index) {
                console.log(current.data);
            }
            count++;
            current = current.next;
        }
        return null;
    }

    //Remove at index
    removeAt(index) {
        let current = this.head,
            previous;
        let count = 0;

        //Remove the first element
        if (index === 0) {
            this.head = current.next;
        } else {
            while (count < index) {
                count++;
                previous = current;
                current = current.next;
            }
            previous.next = current.next;
        }
        this.size--;
    }

    //clear List
    clearList() {
        this.head = null;
        this.size = 0;
    }

    //Print list data
    printListData() {
        let currentNode = this.head;
        while (currentNode) {
            console.log(currentNode.data);
            currentNode = currentNode.next;
        }
    }

    //Print the size of the linked list
    getSize() {
        return this.size;
    }
}

const linkedList = new LinkedList();

linkedList.insertFirst(10);
linkedList.insertFirst(50);
linkedList.insertFirst(100);
linkedList.insertLast(5);
linkedList.insertAt(111, 2);
// linkedList.clearList();
// linkedList.removeAt(3);
// linkedList.getAt(3);
linkedList.printListData();
console.log(`list size is ${linkedList.getSize()}`);
