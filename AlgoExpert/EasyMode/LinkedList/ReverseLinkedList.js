// Given the head of a singly linked list, reverse the list, and return the reversed list.
//Time Complexity -> O(n)
//Space Complexity -> O(1)
//
//Solution
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var reverseList = function (head) {
    //Initialize current, previous, next pointers
    let cur = head,
        prev = null,
        next;

    while (cur !== null) {
        next = cur.next;
        cur.next = prev;
        console.log(cur.next);
        prev = cur;
        cur = next;
    }
    return prev;
};
