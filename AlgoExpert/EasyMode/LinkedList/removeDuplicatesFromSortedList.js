// Given the head of a sorted linked list, delete all duplicates such that each element appears only once.
// Return the linked list sorted as well.
//
//LEETCODE-83
var deleteDuplicates = function (head) {
    let dummy = new ListNode(-Infinity, head);
    let prev = dummy;
    let cur = head;

    while (cur) {
        if (prev.val === cur.val) {
            while (cur && cur.val === prev.val) {
                cur = cur.next;
            }
            prev.next = cur;
        } else {
            prev = cur;
            cur = cur.next;
        }
    }
    return dummy.next;
};
