// Tournament Winner
// E.g competitions = [["HTML", "CSharp"],["C#", "Python"],["Python", "HTML"],] and results = [0, 0, 1]
// O(n) time | O(k) space
const Home_TEAM_WON = 1;
let currentBestTeam = "";
let scores = { currentBestTeam: 0 };

function tournamentWinner(competitions, results) {
    for (const [idx, element] of competitions.entries()) {
        let result = results[idx];
        let [homeTeam, awayTeam] = element;
        winningTeam = result == Home_TEAM_WON ? homeTeam : awayTeam;

        updateScore(winningTeam, 3, scores);
        if (scores[winningTeam] > scores.currentBestTeam) {
            currentBestTeam = winningTeam;
        }
    }
    return currentBestTeam;
}

function updateScore(team, point, scores) {
    if (scores[team] == undefined) {
        scores[team] = 0;
    }
    scores[team] += point;
}
console.log(
    tournamentWinner(
        [
            ["HTML", "CSharp"],
            ["C#", "Python"],
            ["Python", "HTML"],
        ],
        [0, 0, 1]
    )
);
