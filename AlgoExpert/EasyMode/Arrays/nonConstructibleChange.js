// E.g Array=[5,7,1,1,2,3,22]
// Go through the array to figure out how to get a change you cannot create in the array
// SOrt the array in ascending order and add the elements till you compare it to the next element you cannot create a change for.
// Algorithm is if the next element is greater than the last change(adding previous elements) plus 1 : then return the current change plus 1;

//When the Array is not sorted
const Array = [1, 2, 5, 19, 15, 3, 22];
const Array2 = [5, 7, 1, 1, 2, 3, 22];

// function nonConstructibleChange(coins) {
//     let currentChange = 0;
//     for (const coin of coins) {
//         if (coin > currentChange + 1) {
//             return currentChange + 1;
//         }
//         currentChange += coin;
//     }
//     return currentChange;
// }

//When the Array is sorted
function nonConstructibleChange2(coins) {
    sortedCoins = coins.sort(function (a, b) {
        return a - b;
    });
    let currentChange = 0;
    for (const coin of sortedCoins) {
        if (coin > currentChange + 1) {
            return currentChange + 1;
        }
        currentChange += coin;
    }
    return currentChange;
}

console.log(nonConstructibleChange2(Array2));
