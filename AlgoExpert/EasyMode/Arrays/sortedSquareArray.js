// Sorted Square Array
// O(log n) time | O(n) space
const nums = [-6, -4, -1, 0, 3, 10, 11];
// var sortedSquares = function (nums) {
//     let newArr = new Array(nums.length).fill(0);
//     for (let i in nums) {
//         let value = nums[i];
//         newArr[i] = value * value;
//     }
//     newArr = newArr.sort(function (a, b) {
//         return a - b;
//     });
//     return newArr;
// };
var sortedSquares = function (nums) {
    let newArr = new Array(nums.length).fill(0);
    let leftIdx = 0;
    let rightIdx = nums.length - 1;

    for (let i = nums.length - 1; i >= 0; i--) {
        leftVal = nums[leftIdx];
        rightVal = nums[rightIdx];

        if (Math.abs(leftVal) > Math.abs(rightVal)) {
            newArr[i] = leftVal * leftVal;
            leftIdx++;
        } else {
            newArr[i] = rightVal * rightVal;
            rightIdx--;
        }
    }
    return newArr;
};
console.log(sortedSquares(nums));
