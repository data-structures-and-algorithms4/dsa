// Validate Subsequence
// Main_Array [5,1,22,25,6,-1,8,10] and Subsequence[1,6,-1,10]
//////////////////// STEP 3: Figure a solution without code  /////////////////////////
////////////// STEP 3: Figure a solution with code  /////////////////////////////////

// O(n) time | O(1) space
// function validateSubsequence(array, sequence) {
//     arrIdx = 0;
//     seqIdx = 0;
//     while (arrIdx < array.length && seqIdx < sequence.length) {
//         if (array[arrIdx] === sequence[seqIdx]) {
//             seqIdx++;
//         }
//         arrIdx++;
//     }
//     return seqIdx == sequence.length;
// }

// O(n) time | O(1)
function validateSubsequence(array, sequence) {
    seqIdx = 0;
    for (const i in array) {
        if (seqIdx == sequence.length) {
            break;
        }
        if (sequence[seqIdx] == i) {
            seqIdx++;
        }
    }
    return seqIdx == sequence.length;
}

validateSubsequence([5, 1, 22, 25, 6, -1, 8, 10], [1, 6, -1, 10]);
