// Question
// You are given an array of positive integers where each integer represents the height of a vertical line on  a chart.
// Find two lines which together with the x-axis forms a container that would hold the greatest amount of water.
//  Return the area of water it would hold.
//Solution
//////////////////// STEP 1: Verify the constraints   /////////////////////////
// Does the thickness of the line affect the area?
// Do the left and right count as a container
// Does a higher line inside our container affect our area?

//////////////////// STEP 2: Write some test cases   /////////////////////////
// If the array is an empty array? [] = 0
// If the array has only one value? [7] = 0
// If the array has multiple elements that can be the container height? [6,9,3,4,5,8] (6 and 8)6x5=30 or (9 and 8)8*4=32

//////////////////// STEP 3: Figure a solution without code  /////////////////////////
//////////////////// STEP 3: Figure a solution with code  /////////////////////////
const heightArray = [1, 7, 2, 4, 3, 9];
// const getMaxWaterContainer = function (heightArray) {
//     let maxArea = 0;
//     for (let a = 0; a < heightArray.length; a++) {
//         for (let b = a + 1; b < heightArray.length; b++) {
//             const height = Math.min(heightArray[a], heightArray[b]) * (b - a);
//             maxArea = Math.max(maxArea, height);
//         }
//     }
//     return maxArea;
// };

// Optimal solution
const getMaxWaterContainer = function (height) {
    let maxArea = 0,
        p1 = 0,
        p2 = height.length - 1;
    while (p1 < p2) {
        const area = Math.min(height[p1], height[p2]) * (p2 - p1);
        maxArea = Math.max(maxArea, area);
        if (height[p1] <= height[p2]) {
            p1++;
        } else {
            p2--;
        }
    }
    return maxArea;
};

console.log(getMaxWaterContainer(heightArray));
