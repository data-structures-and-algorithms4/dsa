// Question
// Given an array of integers, return the indices of the two numbers that add up to a given target e.g [1,3,7,9,2] -> 11
//Solution
//////////////////// STEP 1: Verify the constraints  /////////////////////////
// Are all numbers positive or can there be a negatives?
// Are there duplicates in the array?
// Will there always be a solution available?
// What do we return if there's no solution??? e.g return null or false etc
// Can there be multiple pairs that add up to the target?

//////////////////// STEP 2: Write Out Test/Edge Cases  /////////////////////////
// If the element not equal to the target? [1,2,3,4,5] t=25
// If the array is empty but target is available [] t=3
// If only one element and target is out of range. [5] t=8
// If only one element and equal to target [5] t=5
// If only elements equals the target [1,6] t=7

//////////////////// STEP 3: Figure a solution without code  /////////////////////////
// target = t;
// p1 = "number which adds to number_to_find to get target";
// Number_to_find = target - Array[p1];
// let findTwoSum = function (target, nums) {
//     for (let p1 = 0; p1 < nums.length; p1++) {
//         console.log({ p1, value: nums[p1] });
//         let number_to_find = target - nums[p1];
//         console.log({ number_to_find });
//         for (let p2 = p1 + 1; p2 < nums.length; p2++) {
//             console.log({ p2, value: nums[p2] });
//             if (number_to_find == nums[p2]) {
//                 return [p1, p2];
//             }
//         }
//     }
//     return null;
// };
let findTwoSum = function (target, nums) {
    const numsMap = {};

    for (let p = 0; p < nums.length; p++) {
        const currentMapVal = numsMap[nums[p]];
        if (currentMapVal >= 0) {
            return [currentMapVal, p];
        } else {
            const number_to_find = target - nums[p];
            numsMap[number_to_find] = p;
        }
    }
    return null;
};

console.log(findTwoSum(18, [1, 3, 7, 9, 2, 6, 9]));

//////////////////// STEP 4: Double check for errors   /////////////////////////
